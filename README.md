# EulerDaily
This project has been discontinuted. Turns out that there already is a daily problem on [ProjectEuler][1] webiste ahahah.

it's a twitter bot which creates a new picture everyday, representing a new randomly selected [ProjectEuler][1].  
it then posts the just created media alongside some informations on the [twitter bot account][2].
  
this bot has no intention of overthrowing codewars or other website of the same kind.  
it's just my way of bringing devs together around fun problem solving.


[1]: (https://www.projecteuler.net)
[2]: (https://twitter.com/EulerDaily)
