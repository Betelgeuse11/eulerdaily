package rand

import (
	"math/rand"
	"time"
)

// Generate a random int between [min, max]
func Int(min, max int) int {
	return int(rand.NewSource(time.Now().UnixNano()).Int63())%(max-min+1) + min
}

// Generate a random uint between [0, max]
func UInt(max int) uint {
	return uint(Int(0, max))
}
