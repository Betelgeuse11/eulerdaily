package image

import "fmt"

const (
	SQUARE_UNIT float64 = 75
	WIDTH       float64 = SQUARE_UNIT * 16
	HEIGHT      float64 = SQUARE_UNIT * 9

	SPACING         float64 = SQUARE_UNIT * 1.5
	MAX_TITLE_WIDTH float64 = float64(WIDTH) - SPACING*2
	LINE_SPACING    float64 = 1.5

	EULER_PID string = "Euler #%d"
)

func getEulerString(pid uint) string { return fmt.Sprintf(EULER_PID, pid) }
