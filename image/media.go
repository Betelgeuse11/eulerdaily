package image

import (
	"image/color"

	"github.com/fogleman/gg"
	"gitlab.com/Betelgeuse11/eulerdaily/euler"
)

type drawString struct {
	value   string
	wrapped bool
	size    [2]float64
}

func (str *drawString) getCenteredX() float64 { return (WIDTH - str.size[0]) * 0.5 }

func calculateStrings(ctx *gg.Context, toDraw ...string) (drawStrings []*drawString, offset float64) {
	var sw, sh float64
	var isWrapped bool

	// Adding the SPACING between each string present in toDraw
	offset += SPACING * float64(len(toDraw)-1)

	for _, str := range toDraw {
		isWrapped = false
		sw, sh = ctx.MeasureString(str)

		if isWrapped = sw > MAX_TITLE_WIDTH; isWrapped {
			sw, sh = ctx.MeasureMultilineString(str, LINE_SPACING)
		}

		drawStrings = append(drawStrings, &drawString{
			value:   str,
			wrapped: isWrapped,
			size:    [2]float64{sw, sh},
		})

		offset += sh
	}

	// Calculate the right offset
	offset = (HEIGHT - offset) * 0.5

	return
}

type colorStop struct {
	position float64
	color    color.RGBA
}

func createBackgroundGradient(stops ...colorStop) gg.Gradient {
	return createGradient(0, 0, WIDTH, HEIGHT, stops...)
}

func createGradient(x, y, w, h float64, stops ...colorStop) gg.Gradient {
	gradient := gg.NewLinearGradient(x, y, w, h)

	for _, stop := range stops {
		gradient.AddColorStop(stop.position, stop.color)
	}

	return gradient
}

func CreateStatusImage(project *euler.Project) {
	ctx := gg.NewContext(int(WIDTH), int(HEIGHT))

	// Change the filling style to a blue shaded gradient
	ctx.SetFillStyle(createBackgroundGradient(
		colorStop{
			position: 0,
			color:    color.RGBA{89, 92, 255, 255},
		},
		colorStop{
			position: 1,
			color:    color.RGBA{189, 248, 255, 255},
		},
	))

	// Fill the background with the newly created gradient using a rectangle
	ctx.DrawRectangle(0, 0, WIDTH, HEIGHT)
	ctx.Fill()

	ctx.SetColor(color.Black)
	// For now everything string is the same size, might change that in the future
	if err := ctx.LoadFontFace("font/Ubuntu-Regular.ttf", SQUARE_UNIT); err != nil {
		panic(err)
	}

	strings, offset := calculateStrings(ctx, getEulerString(project.PID), project.Title)

	for _, str := range strings {
		offset += str.size[1]

		if str.wrapped {
			ctx.DrawStringWrapped(str.value, WIDTH*0.5, offset, 0.5, 0.5, MAX_TITLE_WIDTH, LINE_SPACING, gg.AlignCenter)
			goto addToOffset
		}

		ctx.DrawString(str.value, str.getCenteredX(), offset)

		// Increase the offset (which is used as our Y position) by
		// the height of the current string + the SPACING between strings
	addToOffset:
		offset += SPACING
	}

	ctx.SavePNG("out.png")
}
