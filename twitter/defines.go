package twitter

const (
	UPDATE_STATUS_LINK string = "https://api.twitter.com/1.1/statuses/update.json"
	NEW_MEDIA_LINK     string = "https://upload.twitter.com/1.1/media/upload.json"

	STATUS_BODY string = `Today, let's take a look at project #%d.
	
You can find more informations here
%s

Good luck and have fun !

#developers #maths #algorithms`
)
