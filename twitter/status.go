package twitter

import (
	"fmt"
	"net/http"
	"net/url"
	"os"

	"github.com/dghubble/oauth1"
	"github.com/joho/godotenv"
	"gitlab.com/Betelgeuse11/eulerdaily/euler"
)

func mustGenerateClient() *http.Client {
	// We use this path because we will launch
	// the program from the eulerdaily folder
	err := godotenv.Load("twitter/.env.twitter")
	if err != nil {
		panic(err)
	}

	config := oauth1.NewConfig(os.Getenv("KEY"), os.Getenv("KEY_SECRET"))
	token := oauth1.NewToken(os.Getenv("TOKEN"), os.Getenv("TOKEN_SECRET"))

	return oauth1.NewClient(oauth1.NoContext, config, token)
}

func createStatusMessage(project *euler.Project) string {
	return fmt.Sprintf(
		STATUS_BODY,
		project.PID,
		project.PostURI,
	)
}

func MustSendNewStatus(project *euler.Project) {
	client := mustGenerateClient()

	mediaId := mustCreateNewMedia(client)

	data := url.Values{
		"status":    {createStatusMessage(project)},
		"media_ids": {mediaId},
	}

	resp, err := client.PostForm(UPDATE_STATUS_LINK, data)
	if err != nil {
		panic(err)
	}

	if resp.StatusCode != http.StatusOK {
		panic("The request wasn't successful")
	}
}
