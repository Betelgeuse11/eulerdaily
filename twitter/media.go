package twitter

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
)

/*
	All the media upload and sending has been written with
	the help of this SO issue:
	https://stackoverflow.com/questions/20205796/post-data-using-the-content-type-multipart-form-data
*/

func mustOpenFile(pathname string) *os.File {
	file, err := os.Open(pathname)
	if err != nil {
		panic(err)
	}

	return file
}

func mustCreateNewMedia(client *http.Client) string {
	file := mustOpenFile("out.png")

	body := bytes.Buffer{}
	writer := multipart.NewWriter(&body)

	part, err := writer.CreateFormFile("media", file.Name())
	if err != nil {
		panic(err)
	}

	_, err = io.Copy(part, file)
	if err != nil {
		panic(err)
	}

	writer.Close()

	req, err := http.NewRequest(http.MethodPost, NEW_MEDIA_LINK, &body)
	if err != nil {
		panic(err)
	}

	req.Header.Set("Content-Type", writer.FormDataContentType())

	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	if resp.StatusCode != http.StatusOK {
		r, _ := ioutil.ReadAll(resp.Body)
		fmt.Println(string(r))
		panic(fmt.Sprintf("Something went wrong with the media creation request: %s", resp.Status))
	}

	result := make(map[string]interface{})

	json.NewDecoder(resp.Body).Decode(&result)

	return result["media_id_string"].(string)
}
