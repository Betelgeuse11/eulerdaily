package main

import (
	"gitlab.com/Betelgeuse11/eulerdaily/euler"
	"gitlab.com/Betelgeuse11/eulerdaily/image"
	"gitlab.com/Betelgeuse11/eulerdaily/twitter"
)

func main() {
	project := euler.MustGetProject(nil)

	// Create the soon to be sent image as the out.png file
	image.CreateStatusImage(project)

	// Sent a new status to the twitter API passing through the :
	//	- Project's id
	//	- Link to said project
	//	- Image created especially for this project
	twitter.MustSendNewStatus(project)

	// The generated file "out.png" will be cleaned by the acron task afterward
}
