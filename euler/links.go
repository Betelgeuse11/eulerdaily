package euler

import "fmt"

const (
	lastProjectURI string = "https://projecteuler.net/recent"
	projectURI     string = "https://projecteuler.net/problem=%d"
)

func getProjectURI(pid uint) string {
	return fmt.Sprintf(projectURI, pid)
}
