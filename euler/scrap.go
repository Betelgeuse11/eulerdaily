package euler

import (
	"log"
	"net/http"
	"strconv"

	"github.com/yhat/scrape"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

func parseRoot(uri string) *html.Node {
	resp, err := http.Get(uri)
	if err != nil {
		log.Fatalln(err)
	}

	root, err := html.Parse(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	return root
}

func mustScrapLastProjectNumber() int {
	root := parseRoot(lastProjectURI)
	// All the id_column elements are <td>
	// tds[0] is the first line of the table, which contains the table's titles
	tds := scrape.FindAll(root, scrape.ByClass("id_column"))

	// tds[1].FirstChild corresponds to the text contained inside the td tag
	// Data allows us to retrieve said text
	last, err := strconv.Atoi(scrape.Text(tds[1]))
	if err != nil {
		log.Fatalln(err)
	}

	return last
}

func mustScrapProjectInfos(pid uint) *Project {
	uri := getProjectURI(pid)
	root := parseRoot(uri)

	title, ok := scrape.Find(root, scrape.ByTag(atom.H2))
	if !ok {
		log.Fatalf("No title could be found for the pid %d\n", pid)
	}

	desc, ok := scrape.Find(root, scrape.ByClass("problem_content"))
	if !ok {
		log.Fatalf("No description could be found for the pid %d\n", pid)
	}

	return &Project{
		PID:         pid,
		Title:       scrape.Text(title),
		Description: scrape.Text(desc),
		PostURI:     uri,
	}
}
