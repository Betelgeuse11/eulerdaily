package euler

import (
	"gitlab.com/Betelgeuse11/eulerdaily/rand"
)

type Project struct {
	PID         uint
	Title       string
	Description string
	PostURI     string
	ImageURI    string
}

func getRandomPID(toAvoid []int) (uint, error) {
	last := mustScrapLastProjectNumber()

	pid := rand.UInt(last)

	return pid, nil
}

func MustGetProject(toAvoid []int) *Project {
	pid, err := getRandomPID(toAvoid)
	if err != nil {
		panic(err)
	}

	return mustScrapProjectInfos(pid)
}
