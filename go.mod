module gitlab.com/Betelgeuse11/eulerdaily

go 1.16

require (
	github.com/dghubble/oauth1 v0.7.0
	github.com/fogleman/gg v1.3.0
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/yhat/scrape v0.0.0-20161128144610-24b7890b0945
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d // indirect
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e
)
